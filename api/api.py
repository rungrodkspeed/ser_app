from flask import Flask, jsonify, request 
from flask_cors import CORS
import pickle
import numpy as np

app = Flask(__name__)
cors = CORS(app)

@app.route('/', methods=['POST'])
def func():
    sequence = 'uYHTuyuNusHnudHWUHucuVuLuhHFuCuXuBuSuPHhDNtPuduKHrupDNuIUFEOHFgjEqNxNEatfFsgsxsFFQFXFumoaSszmZmKsFmamhavfHFqFpFjmdmtaGaoftmRaNaMfcFdFLFzFSsdajfyaFfRaRNKXPWwBIaKFXFRFGNoNXBYXPLjVQEpBjWrWUaQazCFGzUCoOFnFzFAFlLTxOUdLiUcEOLkXNFXFGnSlNnwvvPMfRWZaoanXHfRaIfVWxaMXnXAXdBuWpmcFVsFNsamvqWaWpaymnaOvnmDTPmVaDXfXbanTCFJFCFgsvFxWGaOaOXMXeXnXrXlXrQQpljoJvucDBHCUNurEMgZEKHrVlHsDGGeJBTnaamRsNNJFMaqFIadfUQrXtXjXIauaGFjNoFeFvsrshsDsdFPnjkmFgvqvJFefDWFKNauagmSaJfYemmIFsFsFpFRsQsZMfNFaxXXaUXSLCLwKHLUCxHvtoUAHKHPuVDquuDXupDKErunuWHfUKUDukuMHHHSDYUv'
    multiprototype = load_pickle("multiprototype.pickle")
    emotion = emo_map(fknn(sequence, multiprototype))

    return jsonify({"predicted":emotion}), 200


def load_pickle(filename):
    with open(filename, 'rb') as handle:
        temp = pickle.load(handle)
    return temp

def lev(s1, s2):
    if len(s1) < len(s2):
        return lev(s2, s1)

    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1 
            deletions = current_row[j] + 1       
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row
    
    return previous_row[-1]


def emo_map(emotion):
    list_emotion = ["Neutral", "Angry", "Happy", "Sad", "Frustrated"]
    return list_emotion[emotion]

def fknn(unknown, multiprototype, k = 3):
    m= 3
    choose_k = []
    for th_cls, Class in enumerate(multiprototype):
        for n, seq in enumerate(Class):
            dist = lev(unknown, seq)
            if len(choose_k) < k:
                choose_k.append((th_cls, n, dist))
            else:
                for idx, (n_cls, sub_idx, distance) in enumerate(choose_k):
                    if (dist <= distance) and (sub_idx != n):
                        choose_k[idx] = (th_cls, n, dist)
                        break

    u_dist = np.zeros((k,))
    u_class = np.zeros((k,))
    for idx, (Class, sub_idx, dist) in enumerate(choose_k):
        u_dist[idx] = dist
        u_class[idx] = Class

    denominator = np.sum((1 / u_dist) ** (1 / (m-1)))

    Class = np.unique(u_class)
    ui = np.zeros((Class.shape[0],))
    for idx_, Cls in enumerate(Class):
        sigma = 0
        for idx, cls in enumerate(u_class):
            if Cls == cls:
                sigma += ( 1 / u_dist[idx] ) ** (1 / (m-1))
        ui[idx_] = sigma

    prob = ui / denominator

    return int(Class[np.argmax(prob)])

if __name__ == "__main__":
    app.run(debug=True)